composites_fix
==============

Small script that fixes composite-finder output files to use the original FASTA accession-IDs instead of the generated numericical IDs.

Installation
------------

Please install via PIP

Usage
-----

```bash
python -m composites_fix
```

Meta
----

```ini
language=python3
type=application,application-cli
hosts=bitbucket
```