"""
fix_composites:
    Reads a .composites file from stdin, writes a .composites file to stdout.
    Uses a dictionary to translate gene names back to their original values.
         
Usage: python3 fix_composites <dictionary_file>
    <dictionary_file> - Path to dictionary file - standard CSV of value-key pairs.

"""

from sys import stdin, argv, stderr
from os.path import isfile


if len( argv ) != 2 or not isfile(argv[1]):
    print( "Error: No dictionary file specified, or specied item is not a file.", file = stderr )
    print( "", file = stderr )
    print( __doc__, file = stderr )
    exit( 1 )

dictionary_file_name = argv[ 1 ]
dictionary = { }

print( "Reading dictionary...", file = stderr )
with open( dictionary_file_name, "r" ) as dictionary_file:
    for line in dictionary_file:
        line = line.strip()
        value, key = line.split( "\t", 1 )
        dictionary[ key ] = value

print( "Enumerating composites...", file = stderr )
for line in stdin:
    line = line.strip()  # type:str
    
    if line.startswith( ">C" ):
        line = line[ 2: ]
        line = dictionary[ line ]
        print( ">" + line )
    elif line and not "\t" in line:
        line = dictionary[ line ]
        print( line )
    else:
        print( line )

print( "All done!", file = stderr )
exit( 0 )
