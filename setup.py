from distutils.core import setup

setup( name="fix_composites",
       version="1.0",
       description="Renames composite genes to their original accessions",
       author='Martin Rusilowicz',
       packages=[ 'fix_composites' ],
       )
